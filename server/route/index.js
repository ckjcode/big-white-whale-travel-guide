var express = require('express');

var db = require('../utils/db');  //数据库管理工具
var http = require('../utils/http'); //http 管理工具
//应用级中间件绑定到 app 对象 使用
var app = express();
const route = express.Router();

route.get('/index', function(req, res) {
var end = req.query.next_start;
var elements = [];
db.get_trip(end, function (data) {
    if (data == null) {
        http.send(res, -1, "system error");
        return;
    }
    var next_start=0;
    data.forEach(function (item, index, array) {
        var outItem = {
            id: item.id,
            name: item.title,
            cover_image_w640: item.cover_image_w640,
            date_added: item.create_time,
            day_count: (item.end_date - item.start_date) / (24 * 60 * 60 * 1000) + 1,
            view_count: item.view_count,
            user: {
                avatar_l: item.avatarUrl,
                name: item.nickName
            },
            type:4,
            popular_place_str:item.popular_place_str
        };
        next_start=item.id;
        elements.push(outItem);
    });
    res.send({status: 0, data: {elements: elements,next_start:next_start}}).end();

});
})
;

//设置跨域访问
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});


module.exports = route;
