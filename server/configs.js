﻿
exports.mysql = function(){
	return {
		HOST:'127.0.0.1',
		USER:'root',
		PSWD:'123456',
		DB:'travel',
		PORT:3306,
	}
};

exports.wxconfig = function(){
    return {
        appid: '自己的微信小程序信息',
        secret: '自己的微信小程序信息',
        port:8080
    }
};

exports.qiniu = function(){
    return {
        accessKey: '自己的七牛云信息',
        secretKey: '自己的七牛云信息',
        bucketName: 'wxmall'
    }
};
